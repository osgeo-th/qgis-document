.. _`การสร้างพื้นที่ (Boundaries)และนับจำนวน POI ที่สนใจตามพื้นที่ที่ต้องการ`:

24.	การสร้างพื้นที่ (Boundaries)และนับจำนวน POI ที่สนใจตามพื้นที่ที่ต้องการ
========================================================================
**วิธีคิด**

1. เปิดชั้นข้อมูล BKK_branch จากโฟลเดอร์ Lab\Analysis
2. ใช้คำสั่ง Buffer สร้าง Buffer ระยะทาง 500 เมตร กำหนดให้เก็บข้อมูลไปที่ Lab/Analysis ตั้งชื่อไฟล์ BKK_branch_500

.. figure:: img/24_1.png
   :align: center

3. ผลลัพธ์ Buffer 500 เมตร
 
.. figure:: img/24_2.png
   :align: center 

4. ใช้คำสั่ง Intersect สำหรับนับสถานที่สำคัญ (POI) กำหนดให้เก็บข้อมูลที่ Lab/Analysis ตั้งชื่อไฟล์ In_BKK_branch_500

.. figure:: img/24_3.png
   :align: center

5. ผลการวิเคราะห์ KTB ธ.กรุงไทย สาขา ตลาดน้อย มีสถานที่สำคัญ 5 แห่ง คือ
 
   • Bangkok Railway Station = 1 แห่ง
   • 7-Eleven = 2 แห่ง
   • ไม่มีชื่อ (43,30) = 2 แห่ง

 
.. figure:: img/24_4.png
   :align: center
